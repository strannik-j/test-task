from django.urls import path
from rest_framework import views

from .views import StoryView, StoriesView, StoryCoverView

app_name = 'stories'

urlpatterns = [
    path('story/', StoryView.as_view()),
    path('story/<str:story_id>', StoryView.as_view()),
    path('stories/', StoriesView.as_view()),
    path('story_cover/<str:story_id>', StoryCoverView.as_view()),
]