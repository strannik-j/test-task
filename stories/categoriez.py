#!/usr/bin/env python3
import pymorphy2
import numpy as np

from string import punctuation
from nltk.corpus import stopwords
from nltk.tokenize import RegexpTokenizer
from sklearn.feature_extraction.text import CountVectorizer


class Categorizator():
    def __init__(self, input_text):
        self.input_text = input_text
        self.corpus = [
            "обои",
            "мода стиль одежда дизайн ёжик",
            "природа деревья озеро река море гора снег дождь гроза поляна",
            "наука ученый исследование изобретение физика химия астрономия биология космос лаборатория",
            "образование ученик учебник лекция преподаватель институт учитель урок задание оценка экзамен поступление отчисление",
            "чувства отношения любовь страсть горе грусть разлука свидание секс",
            "здоровье болезнь лекарство лечение врач больница скорая помощь смерть",
            "люди человек нация национальность мужчина женщина парень девушка мальчик девочка бабушка дедушка старик старуха брат сестра отец мать",
            "религия атеизм православие христианство католицизм буддизм иудаизм ислам мусульманин язычество",
            "места место город страна",
            "животное зверь птица рыба насекомое собака кошка попугай канарейка лиса волк заяц мышь комар муха",
            "индустрия завод фабрика производство конвеер изделие рабочий",
            "компьютер процессор клавиатура мышь оперативная память монитор дисковод жесткий диск SSD HDD RAM CDROM LAN CPU GPU ROM видеокарта сеть звук windows linux",
            "еда пища продукты питание овощи фрукты риба мясо сосики каша крупа хлеб",
            "спорт соревнование игра олимпиада чемпионат футбол хоккей баскетбол фолейбол теннис регби борьба бокс плавание гимнастика",
            "транспорт автомобиль машина тачка поезд электровоз паровоз самолёт велосипед электричка вагон прицеп грузовик фура",
            "путешествие путешествие достопримечательность турист туризм экскурсия сувенир",
            "здание строение многоэтажка небоскреб высотка",
            "бизнесс работа брокер банк деньги партнер договор аналитика акции облигации IPO ICO рынок торг совещание собрание директор офис",
            "музыка инструмент ноты песня текст исполнитель группа вокал вокалист певец певица музыкант гитара гитарист фортепиано пианино синтезатор клавиши барабан оркестр скрипка виолончель контрабас укулеле бас тенор соло сопрано баритон пение игра габой кларнет домра балалайка саксафон труба флейта туба",
        ]
        self.morph = pymorphy2.MorphAnalyzer()
        self.regex_tok = RegexpTokenizer(r'\w+')
        self.run()


    def tokens_normalization(self):
        tokens = self.regex_tok.tokenize(self.input_text.lower())

        tokens_without_sw = []
        stopwords_list = stopwords.words('russian') + [x for x in punctuation]

        for token in tokens:
            if token not in stopwords_list:
                token = self.morph.parse(token)[0].normal_form
                tokens_without_sw.append(token)
                print(tokens_without_sw)
        return tokens_without_sw

    def run(self):
        vectorizer = CountVectorizer()
        categories = (
        'backgrounds', 'fashion', 'nature', 'science', 'education', 'feelings', 'health', 'people', 'religion',
        'places', 'animals', 'industry', 'computer', 'food', 'sports', 'transportation', 'travel', 'buildings',
        'business', 'music')

        new_text = ' '.join(self.tokens_normalization())
        X = vectorizer.fit_transform(self.corpus)
        N = vectorizer.transform([new_text])
        try:
            a = N.indptr[0]
            b = N.indices[0]
            # print(a, b)
            matrix_freq = np.asarray(X.sum(axis=0)).ravel()
            final_matrix = np.array([np.array(vectorizer.get_feature_names()), matrix_freq])
            main_word = final_matrix[a, b]
            for i in range(0, 20):
                if main_word in self.corpus[i]:
                    return categories[i], main_word
        except:
            return '',''

if __name__ == '__main__':
    # test
    input_text = '''
    У друга живет собака породы колли и длинноухий ёжик. Живут дружно, собака ёжика не обижает, тот мирно топает по ночам. Правда когда ёжик готовится к зимней спячке он буквально сводит собаку с ума! Как только собака приляжет отдохнуть, ёжик подкрадывается к ней и вырывает клок шерсти! Это он себе гнездо строит… На собаку реально больно смотреть! Красные от недосыпа глаза и очень дёрганное поведение…
    '''
    Cat = Categorizator(input_text)
    print(Cat.run())