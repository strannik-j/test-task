import io
import json
import logging.config

from os.path import join
from sys import exc_info
from rest_framework.response import Response
from rest_framework.views import APIView
from pixabay import Image
from django.conf import settings
from .passgen import generate
from .categoriez import Categorizator
from .loggers import loggers_config
from .decorators import print_def_name

logging.config.dictConfig(loggers_config)
log_view = logging.getLogger('log_view')
log_null = logging.getLogger('log_null')


class StoryView(APIView):
    @print_def_name
    def post(self, request):
        story_data = request.data.get("story")
        story_data = self.check_story(story_data)
        if not story_data:
            return Response(status=400, data='Error data')
        story_id = generate(20)
        story_data['cover'] = self.get_cover(story_data['title'], story_data['text'])

        try:
            with open(join(settings.STORIES_DIR,'stories.txt'), 'rt') as f:
                stories_data = json.load(f)
        except (io.UnsupportedOperation, json.decoder.JSONDecodeError, FileNotFoundError) as err:
            log_view.debug(f'stories.txt loading ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            stories_data = {}

        stories_data[story_id] = story_data

        try:
            with open(join(settings.STORIES_DIR,'stories.txt'), 'wt') as f:
                json.dump(stories_data, f, indent=4, ensure_ascii=False)
            return Response(status=201, data=story_id)

        except Exception as err:
            log_view.error(f'stories.txt writing ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')


    @print_def_name
    def put(self, request, story_id):
        story_data = request.data.get("story")

        story_data = self.check_story(story_data, full=False)

        if not story_data:
            return Response(status=400)

        try:
            with open(join(settings.STORIES_DIR,'stories.txt'), "rt") as f:
                stories_data = json.load(f)
        except (io.UnsupportedOperation, json.decoder.JSONDecodeError, FileNotFoundError):
            return Response(status=400, data="None of the stories does not exist. You must first create a story.")

        try:
            cur_story_data = stories_data[story_id]
        except KeyError:
            return Response(status=404, data="There is no story with this ID.")

        log_view.debug(f'story_data={story_data}')

        for key in story_data:
            cur_story_data[key] = story_data[key]
        log_view.debug(f'cur_story_data={cur_story_data}')

        stories_data[story_id] = cur_story_data

        try:
            with open(join(settings.STORIES_DIR, 'stories.txt'), 'wt') as f:
                json.dump(stories_data, f, indent=4, ensure_ascii=False)
            return Response(status=202)

        except Exception as err:
            log_view.error(f'stories.txt writing ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')


    @print_def_name
    def check_story(self, input_data, full=True):
        _data = {}

        for key in ('title', 'author', 'text'):
            try:
                _data[key] = input_data[key]
            except KeyError:
                if full:
                    return False
                else:
                    continue
        return _data


    @print_def_name
    def get_cover(self, title, text):
        image = Image(settings.PIXABAY_KEY)
        categorizator = Categorizator(f'{title} {text}')
        category, main_word = categorizator.run()
        log_view.debug(f'category={category}, main_word={main_word}')
        ims = image.search(q=main_word,
                           lang='ru',
                           image_type='photo',
                           orientation='horizontal',
                           category=category,
                           safesearch='true',
                           order='latest',
                           page=2,
                           per_page=3)
        if ims['hits'] == []:
            log_view.debug('Nothing has been found. Choose a random image.')
            ims = image.search()
        # log_view.debug(f'ims={ims}')
        log_view.debug(f"webformatURL: {ims['hits'][0]['webformatURL']}")
        log_view.debug(f"largeImageURL: {ims['hits'][0]['largeImageURL']}")
        return ims['hits'][0]['webformatURL']


class StoriesView(APIView):
    @print_def_name
    def get(self, request):
        try:
            with open(join(settings.STORIES_DIR,'stories.txt'), "rt") as f:
                stories_data = json.load(f)
        except (io.UnsupportedOperation, json.decoder.JSONDecodeError, FileNotFoundError) as err:
            stories_data = {}
            log_view.debug(
                f'stories.txt loading ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
        return Response(stories_data)


class StoryCoverView(APIView):
    @print_def_name
    def get(self, request, story_id):
        try:
            with open(join(settings.STORIES_DIR,'stories.txt'), "rt") as f:
                stories_data = json.load(f)
        except (io.UnsupportedOperation, json.decoder.JSONDecodeError, FileNotFoundError) as err:
            log_view.debug(
                f'stories.txt loading ERROR: Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')
            return Response(status=400, data="None of the stories does not exist. You must first create a story.")
        try:
            cover = stories_data[story_id]['cover']
        except KeyError:
            log_view.debug(f'There is no story with ID={story_id}')
            return Response(status=404, data="There is no story with this ID.")
        return Response(cover)
