#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  loggers.py
#  
#  Copyright 2020 Strannik-j <strannik-j@mail.ru>
#  
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#  
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#  
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
import logging
from logging.handlers import TimedRotatingFileHandler
from os import getcwd
from os.path import join
from time import strftime
from pathlib import Path
from django.conf import settings

LOG_DIR = join(settings.BASE_DIR, 'log')
Path(LOG_DIR).mkdir(parents=True, exist_ok=True)
print(f'LOG_DIR={LOG_DIR}')

class MegaHandler(logging.Handler):
    def __init__(self, filename):
        logging.Handler.__init__(self)
        self.filename = filename


    def emit(self, record):
        message = self.format(record)
        with open(self.filename, 'a') as file:
            file.write(message + '\n')


loggers_config = {
    'version': 1,
    'disable_existing_loggers': False,

    'formatters': {
        'console': {
            'format': '{asctime} - {levelname}\t- {name}\t- {message}',
            'style': '{'
        },
        'file': {
            'format': '{asctime} - {levelname}\t- {name}\t- {message}',
            'style': '{',
        },
        'null': {
            'format': '',
            'style': '{'
        },

    },
    'handlers': {
        'console': {
            'class': 'logging.StreamHandler',
            'level': 'INFO',
            'formatter': 'console'
        },
        'file_rotate_debug': {
            '()': TimedRotatingFileHandler,
            'level': 'DEBUG',
            'when': 'd',
            'interval': 1,
            'backupCount': 500,
            'filename': join(LOG_DIR, strftime("%Y-%m-%d") + '_debug.log'),
            'formatter': 'file',
            'encoding':'utf_8_sig'
        },
        'file_rotate_info': {
            '()': TimedRotatingFileHandler,
            'level': 'INFO',
            'when': 'd',
            'interval': 1,
            'backupCount': 500,
            'filename': join(LOG_DIR, strftime("%Y-%m-%d") + '_info.log'),
            'formatter': 'file',
            'encoding':'utf_8_sig'
        },
        'file_rotate_error': {
            '()': TimedRotatingFileHandler,
            'level': 'ERROR',
            'when': 'd',
            'interval': 1,
            'backupCount': 500,
            'filename': join(LOG_DIR, strftime("%Y-%m-%d") + '_error.log'),
            'formatter': 'file',
            'encoding':'utf_8_sig'
        },
        'file_debug': {
            '()': MegaHandler,
            'level': 'DEBUG',
            'filename': 'log/debug.log',
            'formatter': 'file',
        },
        'file_info': {
            '()': MegaHandler,
            'level': 'INFO',
            'filename': 'log/info.log',
            'formatter': 'file',
        },
        'file_error': {
            '()': MegaHandler,
            'level': 'ERROR',
            'filename': 'log/error.log',
            'formatter': 'file',
        },
        'null_file_rotate_debug': {
            '()': TimedRotatingFileHandler,
            'level': 'DEBUG',
            'when': 'd',
            'interval': 1,
            'backupCount': 500,
            'filename': join(LOG_DIR, strftime("%Y-%m-%d") + '_debug.log'),
            'formatter': 'null',
            'encoding':'utf_8_sig'
        },
        'null_file_rotate_info': {
            '()': TimedRotatingFileHandler,
            'level': 'INFO',
            'when': 'd',
            'interval': 1,
            'backupCount': 500,
            'filename': join(LOG_DIR, strftime("%Y-%m-%d") + '_info.log'),
            'formatter': 'null',
            'encoding':'utf_8_sig'
        },
        'null_file_rotate_error': {
            '()': TimedRotatingFileHandler,
            'level': 'ERROR',
            'when': 'd',
            'interval': 1,
            'backupCount': 500,
            'filename': join(LOG_DIR, strftime("%Y-%m-%d") + '_error.log'),
            'formatter': 'null',
            'encoding':'utf_8_sig'
        },
        'null_console': {
            'class': 'logging.StreamHandler',
            'level': 'DEBUG',
            'formatter': 'null'
        },
        'null_file_debug': {
            '()': MegaHandler,
            'level': 'DEBUG',
            'filename': 'log/debug.log',
            'formatter': 'null',
        },
        'null_file_info': {
            '()': MegaHandler,
            'level': 'INFO',
            'filename': 'log/info.log',
            'formatter': 'null',
        },
        'null_file_error': {
            '()': MegaHandler,
            'level': 'ERROR',
            'filename': 'log/error.log',
            'formatter': 'null',
        },

    },
    'loggers': {
        'log_view': {
            'level': 'DEBUG',
            'handlers': ['console', 'file_rotate_debug', 'file_rotate_error', 'file_rotate_info']
            # 'propagate': False
        },
        'log_def': {
            'level': 'DEBUG',
            'handlers': ['console', 'file_rotate_debug', 'file_rotate_error', 'file_rotate_info']
            # 'propagate': False
        },
        'log_null': {
            'level': 'DEBUG',
            'handlers': ['null_console', 'null_file_rotate_debug', 'null_file_rotate_error', 'null_file_rotate_info']
            # 'propagate': False
        }
    },

    # 'filters': {},
    # 'root': {}   # '': {}
    # 'incremental': True
}
