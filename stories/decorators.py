#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  my_decorators.py
#
#  Copyright 2020 Strannik-j <strannik-j@mail.ru>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
import functools
import logging.config
from sys import exc_info
from .loggers import loggers_config

logging.config.dictConfig(loggers_config)
log_def = logging.getLogger('log_def')

def print_def_name(func):
    global Verbose
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        log_def.debug(f'{func.__name__}, args={args}, kwargs={kwargs}')
        try:
            out = func(*args, **kwargs)
            return out
        except Exception as err:
            log_def.error(f'Line: {exc_info()[-1].tb_lineno}, {err.__class__.__name__}:  {err}')

            raise err
    return wrapper
